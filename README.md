# Dominoes

##### _Version: 1.0_

This is a web game that simulates players playing the game of Dominoes. After clicking the START button, a deck of 28 dominoes will be generated and shuffled. Each player will then be dealt 7 tiles, a tile from the deck will be placed on the board and the players will begin playing. Each round the active player will check if there's a stalemate (where none of the players can make a legal move and there's no tiles left in the deck), followed by checking if they have any playable tiles. If they don't have any playable tiles, they will repeatedly draw tiles from the deck until they do. They will then place a random playable tile, check if they have won, and if they haven't, the next player's turn will start. Once a player has won, or they have determined that a stalemate occurred, the game will end, and the user will have the opportunity to restart it, by clicking the RESTART button.

### Disclaimer

As the original assignment didn't address stalemates and what to do when one occurs, I found and adapted the rules found on [this](https://www.dominorules.com/draw) this page. It states the following:

> If none of the players can make a play, the game ends in a block. If a game ends in a block, all the players turn the tiles in their hands faceup,
> count the pips on each tile, and add them together. The player with the lowest total wins the game and earns the points (1 point per pip) of all the
> tiles left remaining in his opponents' hands. The player who first reaches 100 points or more is the overall winner.

However, due to time and scope constraints I decided to adapt it to merely add up the pips of all the tiles left in each player's hand and announce the player with the lowest score as the winner, and if all players have the same score the game ends in a draw.

#### Author

Lee Grobler  
+2776 476 9004  
seriouslee131@gmail.com  

## Text Version

I created [this](https://text-45ba4.firebaseapp.com/) version of the game to address the original problem stated in the assignment question, to allow my evaluator to quickly ascertain my ability as a web developer.

### Technology Used

 - HTML
 - CSS
 - Javascript
 - JQuery

I chose these technologies for maximum accessability and ease of use, and to reduce setup time.

### Installation

Simply right click index.html > Open With > choose your preferred browser.

Alternatively, an instance is already running [here](https://text-45ba4.firebaseapp.com/).

### How to Use

Once you've run the application, simply click START and watch the bots dish out a game of Dominoes. Once a winner is announced, you can click RESTART to watch another one.

## Graphical Version

I created [this](https://dominoes-c15a7.web.app/) version of the game to demonstrate the full extent of my development knowledge, to demonstrate the amount of work I can complete in a given time span, and for the pure enjoyment of developing a solution to the stated problem.

### Technology Used

 - HTML
 - Javascript
    - Phaser.js 3
    - Node.js
    - Webpack
    - NPM

### Installation

**Warning:** Requires Node.js and NPM to run locally

In a terminal, navigate to the project root > `/graphical-version`  and run `npm i` and `npm start`. A browser window should have opened automatically, but if one hasn't, copy and paste `http://localhost:8080/` into your preferred browser.

Alternatively, an instance is already running [here](https://dominoes-c15a7.web.app/).

### How to Use

Similarly to the Text Version, simply click START, and RESTART once the game completes.

### Credits
 - [Phaser 3 Webpack Project Template](https://github.com/photonstorm/phaser3-project-template)
 - [Scott Westover](https://phasertutorials.com/author/scottwestover2006/) for [project boilerplate](https://phasertutorials.com/creating-a-phaser-3-template-part-1/) and start button asset.
 - [Domino Pieces](https://opengameart.org/content/dominos)
 - [Large Domino Background Image](https://www.cleanpng.com/png-dominoes-domino-s-pizza-pizza-hut-domino-tiles-dom-3285295/)
 - [Logo Text](https://cooltext.com/)
 - Avatar graphics created by [Noble Master Games](http://www.noblemaster.com/)
 - Avatar graphics designed by [Mei-Li Nieuwland](http://liea.deviantart.com/)
 - [Play/Pause Button](https://opengameart.org/content/medieval-game-button-pack)