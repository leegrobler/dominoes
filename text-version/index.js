const startBtn = $('#start-btn');
startBtn.click(() => {
  // insert a dividing line between games
  if(startBtn.hasClass('restart')) insertDivider();
  start();
  startBtn.prop('disabled', true);
});

// initialise the deck, board and game log
let deck, board, log, players;

// START GAME
const start = () => {
  deck = [];
  board = [];
  log = [];

  // set players array (i wrote the game to allow for 2 or 3 more players, which can be tested by adding more player objects to the players array below)
  players = [
    { name: 'Bob', pronoun: 'He', cantPlay: false, hand: [] },
    { name: 'Alice', pronoun: 'She', cantPlay: false, hand: [] },
  ];

  // check for correct amount of players
  if(players.length <= 1) {
    log.push([`Too few players. Please between 2 and 3 players.`]);
    endGame(0);
    return;
  } else if(players.length > 3) {
    log.push([`Too many players. Please between 2 and 3 players.`]);
    endGame(0);
    return;
  }

  for(let i = 0; i < 7; i++) { // initialise tile deck with all combinations of tiles from 0/0 to 6/6
    for(let j = 0; j < i+1; j++) {
      deck.push({ id: `${j}-${i}`, value: [j, i] });
    }
  }

  deck = shuffleArr(deck);

  for(let i = 0; i < players.length * 7; i++) { // draw 7 tiles for each player
    moveTile(deck, players[i % players.length].hand);
  }

  moveTile(deck, board); // open the game by moving the top tile in the deck to the board

  log.push([`Game starting with first tile: [${board[board.length-1].value.join(':')}]`]);

  play(1); // start the game loop
}

// MAIN GAME LOOP
const play = round => {
  const player = players[round % players.length]; // set player
  let messages = [];

  if(stalemate(messages)) { // check for stalemate and end game loop if there is one
    log.push(messages);
    return endGame(0);
  }

  messages.push(
    `Round ${round}. ${player.name}'s turn.`,
    `Deck: [${deck.map(v1 => v1.value.join(':')).join('] [')}]\n` +
    `Board: [${board.map(v1 => v1.value.join(':')).join('] [')}]\n` +
    `${player.name}: [${player.hand.map(v1 => v1.value.join(':')).join('] [')}]`
  );

  const outerVals = board.reduce((s1, v1, i) => { // determine the board values that the player has to match
    if(i === 0) s1.push(v1.value[0]);
    if(i === board.length - 1) s1.push(v1.value[1]);
    return s1;
  }, []);

  // check if the player has any matching tiles
  let matches = getViableTiles(outerVals, player.hand);

  // draw tiles from the deck until one can be played or the deck's empty
  while(matches.length < 1 && deck.length > 0) {
    moveTile(deck, player.hand);
    messages.push(`${player.pronoun} drew a [${player.hand[player.hand.length-1].value.join(':')}]`);
    matches = getViableTiles(outerVals, player.hand);
  }

  if(matches.length > 0) { // check if a play can be made
    player.cantPlay = false;

    // play a random viable tile
    const playedTile = player.hand.splice(player.hand.indexOf(matches[getRandomNumber(0, matches.length - 1)]), 1)[0];
    
    messages.push(
      `${player.pronoun} can play [${matches.map(v1 => v1.value.join(':')).join('] [')}]\n` +
      `${player.pronoun} plays [${playedTile.value.join(':')}]`
    );

    // potentially flip and add the tile to the matching side of the board
    if(playedTile.value[0] === outerVals[1]) board.push(playedTile);
    else if(playedTile.value[1] === outerVals[0]) board.unshift(playedTile);
    else if(playedTile.value[0] === outerVals[0]) board.unshift(flipTile(playedTile));
    else if(playedTile.value[1] === outerVals[1]) board.push(flipTile(playedTile));

  } else {
    player.cantPlay = true;
    messages.push(`${player.name} couldn't play anything and the deck is empty`);
  }

  if(player.hand.length === 0) { // check for a win
    log.push(messages);
    messages = [`${player.name}: "Domino!"\n${player.name} wins!`];
    log.push(messages);
    return endGame(0);
  }

  log.push(messages);
  return play(round + 1); // recursively call the play function if the game hasn't ended, incrementing the round
};

// recursively logs the game messages to the ui then re-enables the start button
const endGame = async index => {
  const card = $('<div/>').attr({ 'class': 'card' });
  log[index].forEach(v1 => card.append($('<div/>').attr({ 'class': 'message' }).html(v1.replace(/\n/g, '<br />'))));
  $('#main').prepend(card);

  // i added a sleep here to create the illusion of players that think before making a move
  await sleep(getRandomNumber(250, 1000));

  if(index < log.length - 1) return endGame(index + 1);
  else startBtn.addClass('restart').text('RESTART').prop('disabled', false);
};

const moveTile = (origin, destination) => destination.push(origin.pop());

const shuffleArr = arr => arr.map(d => ({ r: Math.random(), d })).sort((a, b) => a.r - b.r).map(d => d.d);

// pauses the execution flow for the specified amount of milliseconds
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

// returns all elements from the hand array that contains values that match any element in the match array
const getViableTiles = (match, hand) => [...new Set(match.map(v1 => hand.filter(v2 => v2.value.reduce((s3, v3) => s3 || v3 === v1, false))).flat(1))];

const flipTile = tile => { // flips a tile object's .value elements around
  tile.value.reverse();
  return tile;
};

// returns a random number within the specified range
const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

// inserts a dividing <hr/> between games
const insertDivider = () => $('#main').prepend($('<hr />'));

// checks if none of the players can make a valid move
const stalemate = messages => {
  // check if all players' cantPlay properties are true, in which case a stalemate has occurred
  const stalemate = players.reduce((s1, v1) => s1 && v1.cantPlay, true);
  if(stalemate) {
    messages.push(`None of the players can make a play thus ending the game in a stalemate.`);
    
    // initialise a score property on each player object to the sum of all the pips on each tile left in their hand
    players = players.map(v1 => {
      return { ...v1, score: v1.hand.reduce((s2, v2) => s2 += v2.value.reduce((s3, v3) => s3 += v3, 0), 0) };
    });

    messages.push(players.map(v1 => `${v1.name} has a score of ${v1.score}`).join('\n'));

    if(players[0].score === players[1].score) { // check if a score draw has occurred
      messages.push(`Because all players have an equal score, the game ends in a draw!`);
    } else {
      const winner = players.sort((a, b) => a.score > b.score ? 1 : -1)[0];
      messages.push(`Because ${winner.name} has the lowest score, ${winner.pronoun.toLowerCase()} wins the game!`);
    }
  }
  
  return stalemate;
};