module.exports = {
  nameTagStyle: { fontFamily: 'Roboto', fontSize: '36px', align: 'center', color: 0x222 }, // font style used for nametags
  cpConfig: { blendMode: 'ADD', scale: { start: 0.4, end: 0 }, speed: { min: 400, max: 600 } }, // common particle effect configs
  characterInfo: JSON.stringify(['Bob', 'John', 'William', 'Alice', 'Lizzy', 'Mary', 'Nancy']), // character names
  sleep: (ms) => new Promise(resolve => setTimeout(resolve, ms)), // pause code execution
  randomNo: (min, max) => Math.floor(Math.random() * (max - min + 1)) + min, // generate random number in range

  randomExcluding: (min, max, exclude) => { // generate random number not included in 'exclude' array
    const rando = Math.floor(Math.random() * (max - min + 1)) + min;
    if(exclude.includes(rando)) {
      return module.exports.randomExcluding(min, max, exclude);
    } else return rando;
  },

  tween: (scene, target, duration, delay = 0, props, onYoyo) => { // animate game object
    return new Promise(resolve => {
      return scene.tweens.add({
        ...props, duration, delay,
        targets: target,
        ease: 'Sine.easeInOut',
        yoyo: !!onYoyo,
        onYoyo: () => onYoyo(),
        onComplete: () => resolve(),
      });
    });
  },
};