import 'phaser';

import config from '../Config/config';
import { tween } from '../helpers';

export default class RoundLabel extends Phaser.GameObjects.Text {
  constructor(scene, round) {
    super(scene, 0, 0, `Round: ${(round + 2) / 2}`, { fontFamily: 'Bangers', fontSize: '96px', stroke: '#33FF14', strokeThickness: 10 });
    
    if(round % 2 === 0) { // display round label every second turn
      this.x = this.randomX();
      this.y = -this.height;
      return this.setOrigin(0.5).scene.add.existing(this).play();      
    } else this.destroy();
  };

  async play() { // animate round label
    await tween(this.scene, this, 500, 0, { x: config.width / 2, y: config.height / 3 }, null);
    await tween(this.scene, this, 500, 1000, { x: this.randomX(), y: config.height + this.height }, null);
    return this.destroy();
  };
  
  randomX() { return Math.floor(Math.random() * (0 - config.width + 1)) + config.width };
};