import 'phaser';

import config from '../Config/config';
import Button from '../Classes/Button';

import { nameTagStyle, cpConfig, tween, randomNo } from '../helpers';

const Angle = Phaser.Math.Angle.Between;

export default class Player extends Phaser.GameObjects.Image {
  constructor(scene, name, no) {
    super(scene, -64, -64, `player-${name.toLowerCase()}`);

    // initialise or set custom properties
    this.p1 = no === 0;
    this.canPlay = true;
    this.hw = this.width / 2;
    this.hh = this.height / 2;
    this.hand = [];
    this.nameTag = this.scene.add.text(-64, -64, name, nameTagStyle).setOrigin(0.5).setDepth(1); // initialise player name tag

    this.setDepth(1).scene.add.existing(this);
  };

  async enter() { // initialise player avatar, name tag and hand position
    const c = [ // set all animation coords
      { x: this.p1 ? config.width + this.width * 2 : -this.width / 2, y: randomNo(config.height, 0) },
      { x: this.p1 ? 48 * 5 + this.hw : config.width - this.hw, y: this.p1 ? this.hh - 30 : config.height - this.hh },
      { x: randomNo(0, config.width + 1), y: this.p1 ? config.height : -this.height },
      { y: this.p1 ? this.height - 10 : config.height - this.height - 10 },
    ];
    const angles = [Angle(c[0].x, c[0].y, c[1].x, c[1].y), Angle(c[2].x, c[2].y, c[1].x, c[3].y)]; // angles between animation coords
    const particles = this.scene.add.particles('flares'); // initialise particle effects

    // flashily animate in avatar
    this.setPosition(c[1].x, c[1].y).setScale(0); // set avatar position and scale
    let follow = this.scene.add.text(c[0].x, c[0].y, 'X', { color: '#c7fcff' }).setOrigin(0.5); // initialise particle starting point
    let emitter = particles.createEmitter({ ...cpConfig, quantity: 2, angle: angles[0], gravityY: 300, follow }); // start movement-particle effect
    await tween(this.scene, follow, 1000, 0, { x: c[1].x, y: c[1].y }).then(() => emitter.explode()); // animate particle movement
    emitter = particles.createEmitter({ ...cpConfig, x: c[1].x, y: c[1].y, quantity: 500 }).explode(); // start explode-particle effect
    tween(this.scene, this, 500, 0, { scale: 1 }); // animate avatar scale

    // flashily animate in name tag
    this.nameTag.setPosition(c[1].x, c[3].y).setScale(0); // set name tag position and scale
    follow.setPosition(c[2].x, c[2].y); // reset particle starting point
    emitter = particles.createEmitter({ ...cpConfig, quantity: 2, angle: angles[1], gravityY: 300, follow }); // start movement-particle effect
    await tween(this.scene, follow, 500, 0, { x: c[1].x, y: c[3].y }).then(() => emitter.explode()); // animate particle movement
    emitter = particles.createEmitter({ ...cpConfig, x: c[1].x, y: c[3].y, quantity: 500 }).explode(); // start explode-particle effect
    tween(this.scene, this.nameTag, 500, 0, { scale: 1 }); // animate name tag scale
  };

  readjust(tilesWide) { // move player 1 to the left and both players' tiles next to them without any gaps
    const x = this.p1 ? tilesWide * 48 + this.hw : this.x;
    const ox = this.p1 ? '+' : '-';
    const oy = this.p1 ? 48 : config.height - 48;

    if(this.p1) { // scooch over player 1's avatar if deck size has decreased enough
      tween(this.scene, this, 500, 0, { x, y: this.y });
      tween(this.scene, this.nameTag, 500, 0, { x, y: this.nameTag.y });
    }

    // animate movement
    return Promise.all(this.hand.map((v1, i) => tween(this.scene, v1, 500, i * 10, { x: eval(x + ox + this.hw + ox + 24 + ox + (i * 48)), y: oy })));
  };

  async play(deck, board) {
    const ovs = board.outerValues(); // get board's exposed values
    const matches = await this.checkForMatches(deck, ovs); // check player has any viable tiles

    this.canPlay = matches.length > 0;
    if(this.canPlay) this.placeTile(matches, ovs, board); // place matching tile, if there is one
  };

  async checkForMatches(deck, outerVals) { // return any playable tiles, or draw until the player has any
    let matches = this.getViableTiles(outerVals);
    if(matches.length < 1 && deck.length > 0) {
      await this.draw(deck.draw());
      return this.checkForMatches(deck, outerVals);
    } else return matches;
  };
  
  // returns all elements from the hand array that contains values that match any element in the match array
  getViableTiles(vals) { return [...new Set(vals.map(v1 => this.hand.filter(v2 => v2.value.reduce((s3, v3) => s3 || v3 === v1, false))).flat(1))]; };

  draw(tile, speed = 500) { // animate the tile's movement to player hand
    if(this.p1) tween(this.scene, tile, speed, 0, { x: this.x + this.hw + 24 + (48 * this.hand.length), y: this.p1 ? 48 : config.height - 48 });
    else tween(this.scene, tile, speed, 0, { x: this.x - this.hw - 24 - (48 * this.hand.length), y: this.p1 ? 48 : config.height - 48 });
    return tween(this.scene, tile, speed / 2, 0, { scaleX: 0 }, () => tile.showFace('face')).then(() => this.hand.push(tile));
  };

  // SIDE NOTE: this function's as non-DRY as I could get it, but it is messy. If someone would like to improve it, be my guest.
  placeTile(matches, ovs, board) {
    const ops = board.outerPositions(); // get board's exposed coordinates
    const tile = this.hand.splice(this.hand.indexOf(matches[randomNo(0, matches.length - 1)]), 1)[0]; // play a random viable tile

    let connected = false, props;
    for(let i = 0; i < 2; i++) { // determine side of played tile and which side of the board it connects to.
      for(let j = 0; j < 2; j++) {
        if(tile.value[j] === ovs[i] && !connected) {
          connected = true;
          const ts = i === 0 ? board[0] : board[board.length - 1];
          const y = i === 0 ? 1 : -1;
          const angle = j === 0 ? -90 : 90;
          const c = { fr: ts.dir === 'fr', rr: ts.dir === 'r' || (!ts.dir && i === 1), vl: ts.dir === 'vl', vr: ts.dir === 'vr', ll: ts.dir === 'l' || (!ts.dir && i === 0), fl: ts.dir === 'fl' };

          if(c.fr) props = { x: ops[i].x + 24, y: ops[i].y + (72 * y), angle, dir: 'r' };
          if(c.rr) props = { x: ops[i].x + 96, y: ops[i].y, angle, dir: ts.x > 512 ? 'vl' : 'r' };
          if(c.vl) props = { x: ops[i].x + 72, y: ops[i].y + (24 * y), angle: angle + (90 * y), dir: 'fl' };
          if(c.vr) props = { x: ops[i].x - 72, y: ops[i].y + (24 * y), angle: angle + (90 * y), dir: 'fr' };
          if(c.ll) props = { x: ops[i].x - 96, y: ops[i].y, angle: angle * -1, dir: ts.x < 336 ? 'vr' : 'l' };
          if(c.fl) props = { x: ops[i].x - 24, y: ops[i].y + (72 * y), angle: angle * -1, dir: 'l' };

          if((i === 1 && j === 1) || (i === 0 && j === 0)) tile.value.reverse();
          if(i === 1) board.push(tile);
          else board.unshift(tile);
        }
      }
    }

    tile.dir = props.dir; // set played tile's direction
    tween(this.scene, tile, 500, 0, { x: props.x, y: props.y, angle: props.angle }); // animate the tile's movement to play area
  };

  // score is calculated as the sum of all the pips on all player's tiles
  calculateScore() { this.score = this.hand.reduce((s1, v1) => s1 += v1.value.reduce((s2, v2) => s2 += v2, 0), 0); };

  async moveToWinPanel(text) {
    tween(this.scene, this, 1000, 0, { x: config.width / 3, y: config.height / 2 - 20 });
    tween(this.scene, this.nameTag, 1000, 0, { x: 475, y: config.height / 2 - 40 }).then(() => {
      this.nameTag.setText(this.nameTag.text + text);
      new Button(this.scene, 475, config.height / 2 + 60, 'menu-btn-1', 'menu-btn-2', () => this.scene.scene.start('Game'), 0.3, 'RESTART');
    });
  };
};