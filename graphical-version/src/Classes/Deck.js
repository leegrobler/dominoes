import 'phaser';

import config from '../Config/config';
import Domino from './Domino';

import { tween } from '../helpers';

// TODO: convert this to extend Array instead of Group

export default class Deck extends Array {
  constructor(scene) {
    super();
    this.scene = scene;
  };

  create() { // initialise tiles
    let shown = [];
    let boneyard = [];

    for(let i = 0; i < 6; i++) { // shown & boneyard coords
      for(let j = 0; j < 7; j++) {
        if(i < 4) shown.push({ x: parseInt(j * 100 + config.width / 8.5), y: i * 96 + config.height / 4 });
        if(j < 5) boneyard.push({ x: j * 48 + 24, y: i * 96 + 48 });
      }
    }

    // shuffled coords
    const shuffled = shown.map(d => ({ r: Math.random(), d })).sort((a, b) => a.r - b.r).map(d => d.d);

    for(let i = 0; i < 7; i++) { // generate deck
      for(let j = 0; j < i+1; j++) {
        this.push(new Domino(this.scene, i, j, [shown[this.length], shuffled[this.length], boneyard[this.length]]));
      }
    }
    return this;
  };

  shuffle() {
    return Promise.all(this.map(async (v1, i) => {
      await tween(this.scene, v1, 500, i * 10, { x: v1.coords[0].x, y: v1.coords[0].y }); // display tiles
      await tween(this.scene, v1, 250, 500 + (i * 10), { scaleX: 0 }, () => v1.showFace('back')); // flip tiles
      await tween(this.scene, v1, 500, 500 + (i * 10), { x: v1.coords[1].x, y: v1.coords[1].y }); // shuffle tiles
      await tween(this.scene, v1, 500, 500 + (i * 10), { x: v1.coords[2].x, y: v1.coords[2].y }); // move tiles to deck area
    })).then(() => Phaser.Actions.Shuffle(this));
  };

  // return the last tile from the deck array
  draw() { return this.pop(); };

  readjust() { // move the remaining tiles to the left
    const coords = [];
    let ext = this.length % 6;
    this.tilesWide = parseInt(this.length / 6) + Number(!!ext);

    for(let i = 0; i < 6; i++) {
      for(let j = 0; j < parseInt(this.length / 6) + Number(!!ext); j++) {
        coords.push({ x: j * 48 + 24, y: i * 96 + 48 });
      }
      if(ext > 0) ext--;
    }

    return Promise.all(this.map((v1, i) => tween(this.scene, v1, 500, i * 10, coords[i])));
  };
};