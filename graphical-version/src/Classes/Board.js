import 'phaser';

import config from '../Config/config';
import { tween } from '../helpers';

export default class Board extends Array {
  constructor(scene, tilesWide) {
    super();
    this.scene = scene;
  };

  readjust(tilesWide) { // move the played tiles to play area's center while keeping its structure
    this.setPaCenter(tilesWide);
    this.setTCenter(tilesWide);
    return Promise.all(this.map((v1, i) => tween(this.scene, v1, 500, i * 10, { x: `+=${this.bcx - this.tcx}`, y: `+=${this.bcy - this.tcy}` })));
  };

  setPaCenter(tilesWide) { // set center point of play area
    this.width = config.width - (tilesWide * 48);
    this.bcx = (tilesWide * 48) + (this.width / 2); // boardCenterX
    this.bcy = (config.height / 2) - 10;
  };

  setTCenter() { // set center point of played tiles
    const bounds = { left: config.width, right: 0, top: config.height, bot: 0 };
    this.forEach(v1 => {
      if(v1.y < bounds.top) bounds.top = v1.y;
      if(v1.y > bounds.bot) bounds.bot = v1.y;
      if(v1.x < bounds.left) bounds.left = v1.x;
      if(v1.x > bounds.right) bounds.right = v1.x;
    });

    this.tcx = (bounds.right + bounds.left) / 2; // tilesCenterX
    this.tcy = (bounds.top + bounds.bot) / 2;
  };

  open(tile) { // take and animate opening tile
    tween(this.scene, tile, 500, 0, { x: this.bcx, y: this.bcy, angle: -90 });
    return tween(this.scene, tile, 250, 10, { scaleX: 0 }, () => tile.showFace('face')).then(() => this.push(tile));
  };

  // return the exposed values that the players have to match
  outerValues() { return [this[0].value[0], this[this.length - 1].value[1]]; };

  outerPositions() { // return the exposed values' coordinates
    return [
      { x: this[0].x, y: this[0].y },
      { x: this[this.length - 1].x, y: this[this.length - 1].y },
    ];
  };
};