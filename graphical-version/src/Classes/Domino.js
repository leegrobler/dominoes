import 'phaser';

export default class Domino extends Phaser.GameObjects.Image {
  constructor(scene, val1, val2, coords) {
    super(scene, 24, 48, `tile-${val2}_${val1}`);

    // set initial properties
    this.face = `tile-${val2}_${val1}`;
    this.back = `tile-back`;
    this.value = [val2, val1];
    this.coords = coords;
    this.dir = null; // direction

    this.setScale(0.1875).scene.add.existing(this);
  };

  // flip tile
  showFace(side) { this.setTexture(this[side]); };
};