import 'phaser';

export default class GameButton extends Phaser.GameObjects.Image {
  constructor(scene, x, y, key1, key2, click, scale = 1, label) {
    super(scene, x, y, key1);
    this.setScale(scale).setInteractive().scene.add.existing(this);

    // define button events
    this.on('pointerover', () => this.setTexture(key2));
    this.on('pointerout', () => this.setTexture(key1));
    this.on('pointerdown', () => click());

    if(label) { // used with text buttons
      const style = { fontFamily: 'Roboto', fontSize: '24px', align: 'center', color: '#fff', ...label.style };
      this.label = this.scene.add.text(this.x, this.y, label.text || label, style).setOrigin(0.5);
      this.label.setDepth(1);
    }
  };
};