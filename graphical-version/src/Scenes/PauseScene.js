import 'phaser';

import config from '../Config/config';
import Button from '../Classes/Button';

export default class PauseScene extends Phaser.Scene {
  constructor() {
    super('Pause');
  };

  create() { // create resume and restart buttons
    ['play', 'restart'].forEach((v1, i) => {
      new Button(this, config.width - 25 - (i * 35), 25, `${v1}-btn-1`, `${v1}-btn-2`, () => {
        this.scene.stop();
        return i === 0 ? this.scene.resume('Game') : this.scene.start('Game');
      }, 0.15)
    });
  };
};
