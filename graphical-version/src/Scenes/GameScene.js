import 'phaser';

import Deck from '../Classes/Deck';
import Player from '../Classes/Player';
import Board from '../Classes/Board';
import RoundLabel from '../Classes/RoundLabel';
import Button from '../Classes/Button';

import { cpConfig, randomNo, characterInfo, tween } from '../helpers';
import config from '../Config/config';

export default class GameScene extends Phaser.Scene {
  constructor() {
    super('Game');
  };

  preload() { // load game fonts and set background color
    this.load.rexWebFont({ google: { families: ['Roboto', 'Bangers'] }});
    this.cameras.main.setBackgroundColor(0xc7fcff);
  };

  async create() {
    new Button(this, config.width - 25, 25, 'pause-btn-1', 'pause-btn-2', () => { // display pause button
      window.game.scene.pause('Game');
      this.scene.launch('Pause');
    }, 0.15);

    // initialise game objects and shuffle deck
    this.board = new Board(this);
    this.deck = new Deck(this).create();
    await this.deck.shuffle();

    // initialise players, let them enter, deal their starting hand
    const players = JSON.parse(characterInfo);
    this.players = [0, 1].map(v => new Player(this, players.splice(randomNo(0, players.length - 1), 1)[0], v));
    for(let i = 0; i < 2; i++) await this.players[i].enter();
    for(let i = 0; i < 14; i++) await this.players[i % 2].draw(this.deck.draw(), 250);

    // readjust remaining deck tiles and players
    this.deck.readjust();
    await this.players[0].readjust(this.deck.tilesWide);

    // set play area center and set starting tile
    this.board.setPaCenter(this.deck.tilesWide);
    await this.board.open(this.deck.draw());

    this.play(0); // start game loop
  };

  async play(round) { // main game loop
    await new RoundLabel(this, round); // display round label
    await this.players[round % 2].play(this.deck, this.board); // take turn

    // readjust deck, players and board
    await this.deck.readjust();
    this.players[round % 2].readjust(this.deck.tilesWide);
    await this.board.readjust(this.deck.tilesWide);

    if(this.players[round % 2].hand.length === 0) { // check if someone's won
      return this.gameover(this.players[round % 2], `\nhas won!`);
    }

    if(!this.players[0].canPlay && !this.players[1].canPlay) { // check for stalemate
      this.players.forEach(v1 => v1.calculateScore());
      return this.gameover(this.players.sort((a, b) => a.score < b.score ? -1 : 1)[0], `\nwon with the\nlowest score!`);
    }

    return this.play(round + 1);
  };

  gameover(player, text) { // display end-of-game panel
    const c = { x: config.width / 2, y: config.height / 2 };
    [0x222222, 0xcaffc7].map((v1, i) => tween(this, this.add.rectangle(c.x + i * 5, c.y + i * 5, 500, 250, v1).setScale(0), 500, 0, { scale: 1 }));
    this.add.particles('flares').createEmitter({ ...cpConfig, x: c.x, y: c.y, quantity: 500 }).explode(); // make it flashy :P
    player.moveToWinPanel(text);
  };
};