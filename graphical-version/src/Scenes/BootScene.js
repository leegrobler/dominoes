import 'phaser';

export default class BootScene extends Phaser.Scene {
  constructor () {
    super('Boot');
  };

  preload () { // load the assets needed in load scene
    this.load.image('splash', require('../assets/splash.png'));
    this.load.image('logo-1', require('../assets/logo-1.png'));
  };

  // start load scene
  create () { this.scene.start('Preloader'); };
};