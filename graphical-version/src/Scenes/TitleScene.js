import 'phaser';

import config from '../Config/config';
import Button from '../Classes/Button';

export default class TitleScene extends Phaser.Scene {
  constructor () {
    super('Title');
  }

  preload() { // load game fonts and display menu images
    this.load.rexWebFont({ google: { families: ['Roboto', 'Bangers'] }}); 

    this.add.image(config.width / 2, 0, 'splash').setOrigin(0);
    this.add.image(config.width / 2, config.height / 4, 'logo-1').setOrigin(0.5);
    this.add.image(config.width / 2, config.height - 109 / 2, 'logo-2').setOrigin(0.5);
  };

  // display the button that starts the game scene
  create() { new Button(this, config.width/2, config.height/2, 'menu-btn-1', 'menu-btn-2', () => this.scene.start('Game'), 0.3, 'START'); };
};