import 'phaser';
import { tween } from '../helpers';
import config from '../Config/config';

export default class PreloaderScene extends Phaser.Scene {
  constructor () {
    super('Preloader');
  };

  preload () {
    // display menu images
    this.add.image(config.width / 2, 0, 'splash').setOrigin(0);
    this.add.image(config.width / 2, config.height / 4, 'logo-1').setOrigin(0.5);

    // display progress bar
    let progressBar = this.add.graphics();
    let progressBox = this.add.graphics().fillStyle(0x222222, 0.8).fillRect(240, config.height / 2 - 25, 320, 50);

    // display progress text
    const style = (size) => ({ font: `${size}px monospace`, fill: '#ffffff' });
    let loadingTxt = this.make.text({ x: config.width / 2, y: config.height / 2 - 50, text: 'Loading...', style: style(20) }).setOrigin(0.5);
    let percentTxt = this.make.text({ x: config.width / 2, y: config.height / 2, text: '0%', style: style(18) }).setOrigin(0.5);
    let assetTxt = this.make.text({ x: config.width / 2, y: config.height / 2 + 50, text: '', style: style(18) }).setOrigin(0.5);

    // update progress bar
    this.load.on('progress', val => {
      percentTxt.setText(parseInt(val * 100) + '%');
      progressBar.clear();
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(250, config.height / 2 - 15, 300 * val, 30);
    });

    // update file progress text
    this.load.on('fileprogress', file => assetTxt.setText('Loading asset: ' + file.key));

    // remove progress bar when complete
    this.load.on('complete', async () => {
      progressBar.destroy();
      progressBox.destroy();
      loadingTxt.destroy();
      percentTxt.destroy();
      assetTxt.destroy();

      // display the logo subtitle before starting the title scene
      const credit = this.add.image(config.width / 2, -109, 'logo-2').setOrigin(0.5);
      await tween(this, credit, 3000, 0, { y: config.height - (credit.height / 2) });
      this.scene.start('Title');
    });

    // load assets needed in game
    this.load.atlas('flares', require('../assets/flares.png'), require('../assets/flares.json'));
    this.load.image('logo-2', require('../assets/logo-2.png'));
    this.load.image('menu-btn-1', require('../assets/buttons/menu-btn-1.png'));
    this.load.image('menu-btn-2', require('../assets/buttons/menu-btn-2.png'));
    this.load.image('pause-btn-1', require('../assets/buttons/pause-btn-1.png'));
    this.load.image('pause-btn-2', require('../assets/buttons/pause-btn-2.png'));
    this.load.image('play-btn-1', require('../assets/buttons/play-btn-1.png'));
    this.load.image('play-btn-2', require('../assets/buttons/play-btn-2.png'));
    this.load.image('restart-btn-1', require('../assets/buttons/restart-btn-1.png'));
    this.load.image('restart-btn-2', require('../assets/buttons/restart-btn-2.png'));
    this.load.image('player-bob', require('../assets/characters/bob.png'));
    this.load.image('player-john', require('../assets/characters/john.png'));
    this.load.image('player-william', require('../assets/characters/william.png'));
    this.load.image('player-alice', require('../assets/characters/alice.png'));
    this.load.image('player-lizzy', require('../assets/characters/lizzy.png'));
    this.load.image('player-mary', require('../assets/characters/mary.png'));
    this.load.image('player-nancy', require('../assets/characters/nancy.png'));
    this.load.image('tile-back', require('../assets/dominos/back.png'));
    for(let i = 0; i < 7; i++) {
      for(let j = 0; j < i+1; j++) {
        this.load.image(`tile-${j}_${i}`, require(`../assets/dominos/${j}-${i}.png`));
      }
    }
  };
};