// NPM MODULES
import 'phaser';
import "regenerator-runtime/runtime.js";

// CLASSES
import config from './Config/config';
import BootScene from './Scenes/BootScene';
import PreloaderScene from './Scenes/PreloaderScene';
import TitleScene from './Scenes/TitleScene';
import GameScene from './Scenes/GameScene';
import PauseScene from './Scenes/PauseScene';

class Game extends Phaser.Game {
  constructor () { // initialise game scenes
    super(config);
    this.scene.add('Boot', BootScene);
    this.scene.add('Preloader', PreloaderScene);
    this.scene.add('Title', TitleScene);
    this.scene.add('Game', GameScene);
    this.scene.add('Pause', PauseScene);
    this.scene.start('Boot');
  };
};

window.game = new Game(); // start game instance
