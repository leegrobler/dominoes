import 'phaser';

import WebfontLoaderPlugin from 'phaser3-rex-plugins/plugins/webfontloader-plugin';

export default { // GAME CONFIG
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: 800,
  height: 600,
  plugins: {
    global: [{ key: 'rexWebfontLoader', plugin: WebfontLoaderPlugin, start: true }]
  }
};